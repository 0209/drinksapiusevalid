package com.devcamp.drinks.controller;

import com.devcamp.drinks.model.CDrink;
import com.devcamp.drinks.respository.CDrinkRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkDeleteAllController {
    
    @Autowired
    CDrinkRespository pDrinkRespository;

    @DeleteMapping("/drinks")
    public ResponseEntity<CDrink> deleteAllDrink(){
        try{
            pDrinkRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
