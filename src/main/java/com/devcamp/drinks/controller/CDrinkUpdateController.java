package com.devcamp.drinks.controller;

import java.util.Date;

import com.devcamp.drinks.model.CDrink;
import com.devcamp.drinks.respository.CDrinkRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkUpdateController {
    
    @Autowired
    CDrinkRespository drinkRespository;

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @RequestBody CDrink pDrinks){
        try {
            CDrink drinkData = drinkRespository.findById(id);
            CDrink drink = drinkData;
            drink.setMaNuocUong(pDrinks.getMaNuocUong());
            drink.setTenNuocUong(pDrinks.getTenNuocUong());
            drink.setDonGia(pDrinks.getDonGia());
            drink.setGhiChu(pDrinks.getGhiChu());
            drink.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(drinkRespository.save(drink),HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("Failed to update specified drink" + e.getCause().getCause().getMessage());
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
