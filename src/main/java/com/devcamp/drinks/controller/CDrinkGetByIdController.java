package com.devcamp.drinks.controller;


import java.util.Optional;

import com.devcamp.drinks.model.CDrink;
import com.devcamp.drinks.respository.CDrinkRespository;

import org.apache.catalina.webresources.DirResourceSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkGetByIdController {
    
    @Autowired
    CDrinkRespository drinkRespository;

    @GetMapping("/drinks/{id}")           
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id){    
        
        try {
            CDrink drinkData = drinkRespository.findById(id);
            return new ResponseEntity<>(drinkData,HttpStatus.OK);  
        }catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
