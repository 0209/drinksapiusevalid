package com.devcamp.drinks.controller;

import java.util.List;

import com.devcamp.drinks.model.CDrink;
import com.devcamp.drinks.respository.CDrinkRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkController {
    
    @Autowired
    CDrinkRespository drinkRespository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){
        try {
            List<CDrink> allDrinks = new ArrayList<>();

            drinkRespository.findAll().forEach(allDrinks::add);
            return new ResponseEntity<>(allDrinks,HttpStatus.OK);
        } catch(Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
