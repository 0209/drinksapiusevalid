package com.devcamp.drinks.controller;

import java.util.Date;

import javax.validation.Valid;

import com.devcamp.drinks.model.CDrink;
import com.devcamp.drinks.respository.CDrinkRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkCreateController {
    
    @Autowired
    CDrinkRespository drinkRespository;

    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrinks){
        try{
            pDrinks.setNgayTao(new Date());
            pDrinks.setNgayCapNhat(null);
            CDrink _drink = drinkRespository.save(pDrinks);
            return new ResponseEntity<>(_drink,HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to create spectifed drink" + e.getCause().getCause().getMessage());
        }
    }
}
