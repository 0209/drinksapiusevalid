package com.devcamp.drinks.respository;

import com.devcamp.drinks.model.CDrink;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CDrinkRespository extends JpaRepository<CDrink,Long> {
    CDrink findById(long id);
}
